# Exercício de avaliação de VueJS

## Instalação da API

### Clone do repoditório
```
git clone https://bitbucket.org/formacaoatec/sjm-vuejs.git
```

### Instalar dependências
```
composer install
```

### Criação da base de dados e configuração
Após criar a base de dados, configure o ficheiro .env

### Executar migrations
```
php artisan migrate --seed
```

### Arrancar servidor
```
php artisan serve
```
## Uso da API

### Obter todos os filmes
```
GET /api/movies
```

### Obter um filme específico
```
GET /api/movies/{id}
```

### Criar um filme
```
POST /api/movies
{
    "title": "Avatar",
    "genre": "Sci-Fi",
    "year": 2009
}
```

### Modificar um filme
```
PUT /api/movies/{id}
{
    "title": "Avatar",
    "genre": "Sci-Fi",
    "year": 2009
}
```

### Arquivar um filme
```
PUT /api/movies/{id}/archive
```

### Modificar um filme
```
PUT /api/movies/{id}/unarchive
```

### Eliminar um filme
```
DELETE /api/movies/{id}
```
