<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = ['title', 'year', 'genre'];

    protected $hidden = ['created_at', 'updated_at'];
}
