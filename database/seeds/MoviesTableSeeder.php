<?php

use Illuminate\Database\Seeder;
use App\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Movie::create([
            'title' => 'The Shawshank Redemption',
            'year' => '1994',
            'genre' => 'Drama'
        ]);
        Movie::create([
            'title' => 'Okja',
            'year' => '2017',
            'genre' => 'Adventure'
        ]);
        Movie::create([
            'title' => 'The Godfather',
            'year' => '1972',
            'genre' => 'Drama'
        ]);
        Movie::create([
            'title' => 'Wonder Woman',
            'year' => '2017',
            'genre' => 'Action'
        ]);
        Movie::create([
            'title' => 'The Dark Knight',
            'year' => '2008',
            'genre' => 'Action'
        ]);
        Movie::create([
            'title' => 'Schindler\'s List',
            'year' => '1993',
            'genre' => 'Drama'
        ]);
        Movie::create([
            'title' => 'Dunkirk',
            'year' => '2017',
            'genre' => 'War'
        ]);
    }
}
